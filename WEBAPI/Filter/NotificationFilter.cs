﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace XamarinLogin.Models
{
    public class NotificationFilter : AuthorizationFilterAttribute
    {

        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (!IsUserAuthorized(filterContext))
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                return;
            }
            base.OnAuthorization(filterContext);
        }
        public bool IsUserAuthorized(HttpActionContext actionContext)
        {
            var authHeader = FetchFromHeader(actionContext);
            if (authHeader != null)
            {
                //Validate JWT
                var tokVal = TokenNotificationManager.ValidateToken(authHeader);
                if (tokVal == null)
                {
                    return false;
                }
                return true;
            }
            return false;
        }
        private string FetchFromHeader(HttpActionContext actionContext)
        {
            string requestToken = null;

            var authRequest = actionContext.Request.Headers.Authorization;
            if (authRequest != null && authRequest.Scheme == "Bearer")
            {
                requestToken = authRequest.Parameter.ToString();
            }
            return requestToken;
        }

    }
}
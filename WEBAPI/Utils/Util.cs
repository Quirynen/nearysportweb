﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using XamarinLogin.Models;

namespace XamarinLogin.Utils
{
    public static class Util
    {
        /// <summary>
        /// Send mail with an unique code to retrieve password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="content"></param>
        public static void sendMail(string email, string content)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("NearySport@gmail.com");
            mail.To.Add(email);
            mail.Subject = "NearySport recovering Password";
            mail.IsBodyHtml = true;
            mail.Body = " Copy the generated password below and enter it in the application, you will be able to choose another password, This code will expire in 5 minutes : "
                           + "</br>" + "<strong>" + content + " </strong> ";
            SmtpServer.Port = 587;
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.EnableSsl = true;
            SmtpServer.Credentials = new System.Net.NetworkCredential("NearySport@gmail.com", "44782kE2");
            SmtpServer.Send(mail);
        }
        /// <summary>
        /// Select userId from JWT
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static int getUserId(string token)
        {
            if(token.Contains("Bearer")){
                token = token.Replace("Bearer", "");
                token=token.Replace(" ", "");
            }
            if (token == null)
            {
                return -1;
            }
            var tokVal = TokenManager.ValidateToken(token);
            if (tokVal == null)
            {
                return -1;
            }
            return Int32.Parse(tokVal);
        }
        /// <summary>
        /// Create geography point for a given lat, lon
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <param name="srid"></param>
        /// <returns></returns>
        public static DbGeography CreatePoint(double lat, double lon, int srid = 4326)
        {
            string wkt = String.Format("POINT({0} {1})", lon, lat);

            return DbGeography.PointFromText(wkt, srid);
        }
        /// <summary>
        /// Get JWT key
        /// </summary>
        /// <returns></returns>
        private static string GetSecurityKey()
        {
            string result = System.Configuration.ConfigurationManager.AppSettings["JWTKEY"];
            return result;
        }

        private static byte[] GetSymmetricSecurityKeyAsBytes()
        {
            var issuerSigningKey = GetSecurityKey();
            byte[] data = Encoding.UTF8.GetBytes(issuerSigningKey);
            return data;
        }

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            byte[] data = GetSymmetricSecurityKeyAsBytes();
            var result = new SymmetricSecurityKey(data);
            return result;
        }
        
    }
}

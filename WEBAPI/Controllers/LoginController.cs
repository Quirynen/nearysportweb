﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using XamarinLogin.Models;
using XamarinLogin.Utils;
using Shared_Model_nearySport;
using Shared_Model_nearySport.Model;

namespace XamarinLogin.Controllers
{
    [JWTAuthenticationFilter]
    /// <summary>
    /// LoginController
    /// </summary>
    public class LoginController : ApiController
    {
        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();

        /// <summary>
        ///     Add new user
        /// </summary>
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/Login/CreateProfil")]
        [System.Web.Http.ActionName("XAMARIN_REG")]
        [System.Web.Http.AllowAnonymous]
        public HttpResponseMessage Xamarin_reg(ProfilUser login)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
            }
            try
            {
                    if (login.Password.Length < 8 || login.Password.Length > 15)
                        return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable,
                            "Password must have between 8 and 15 chars");
                    var hash = SecurePasswordHasher.Hash(login.Password);
                    var user = new Users
                    {
                        UserName = login.UserName,
                        Password = hash
                    };
                    var result = _db.CheckExist(user.UserName);
                    if (result.Any())
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                            "Username already took, please try another one");
                        try
                        {
                            _db.Users.Add(user);
                            _db.SaveChanges();
                        }
                        catch (DbEntityValidationException ex)
                        {
                            return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Profil not added");
                        }
                        var profil = new Profil
                        {
                            Mail = login.Email,
                            Town = login.Town,
                            Fiability = 10,
                            UserId = user.UserId
                        };
                        var controller = DependencyResolver.Current.GetService<ProfilController>();
                        controller.ControllerContext = ControllerContext;
                        HttpResponseMessage response;
                        response = controller.Xamarin_regProfil(profil);
                        if (response.StatusCode == HttpStatusCode.Created)
                            return Request.CreateResponse(HttpStatusCode.Created, "Created successfully");

                        _db.DeleteUser(user.UserId);
                        return Request.CreateResponse(HttpStatusCode.Conflict,
                            response.Content.ReadAsStringAsync().Result);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Update an existing password, when the user is logged in
        /// </summary>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/Login")]
        [System.Web.Http.ActionName("Xamarin_UpdatePassword")]
        public HttpResponseMessage Xamarin_UpdatePassword(string password)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.ToString());
                if (password != null)
                {
                    var hash = SecurePasswordHasher.Hash(password);
                    _db.UpdatePasswordl(hash, userId);
                    _db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Password updated");
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, "Password was empty, not updated");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Update a existing password when the previous has been lost
        /// </summary>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/Login/CreateNewPassword")]
        [System.Web.Http.ActionName("Xamarin_UpdatePasswordNew")]
        [System.Web.Http.AllowAnonymous]
        public HttpResponseMessage Xamarin_CreateNewPassword(string password)
        {
            try
            {
                var guid = Request.Headers.Authorization.ToString();
                if (guid == null) return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Token");
                var tokVal = _db.GetUserIdFromGuid(guid).First();
                if (tokVal == null) return Request.CreateResponse(HttpStatusCode.NotFound, "Session expired");
                var userId = int.Parse(tokVal.ToString());
                if (password != null)
                {
                    var hash = SecurePasswordHasher.Hash(password);
                    _db.UpdatePasswordl(hash, userId);
                    _db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Password updated");
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, "Enter a valid password");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Check if credentials are correct, if so : return JWT
        /// </summary>
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Login")]
        [System.Web.Http.ActionName("XAMARIN_Login")]
        [System.Web.Http.AllowAnonymous]
        public HttpResponseMessage Xamarin_login(string username, string password)
        {
            try
            {
                if (username != null && password != null)
                {
                    var hash = SecurePasswordHasher.Hash(password);
                    var result = SecurePasswordHasher.Verify(password, hash);
                    if (result == false)
                        return Request.CreateResponse(HttpStatusCode.NotFound,
                            "Please Enter valid UserName And/Or Password");
                    var user = _db.Users.Where(x => x.UserName == username).FirstOrDefault();
                    if (user == null)
                        return Request.CreateResponse(HttpStatusCode.NotFound,
                            "Please Enter valid UserName And/Or Password");
                    if (SecurePasswordHasher.Verify(password, user.Password))
                    {
                        var token = TokenManager.GenerateToken(user.UserId);
                        var tokenNotif = TokenNotificationManager.GenerateToken(user.UserId);
                        var listToken = new List<string>();
                        listToken.Add(token);
                        listToken.Add(tokenNotif);
                        return Request.CreateResponse(HttpStatusCode.Accepted, listToken);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        "Please Enter valid UserName And/Or Password");
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, "Please enter your credentials");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Delete an existing user, via JWT
        /// </summary>
        [System.Web.Http.HttpDelete]
        [System.Web.Http.ActionName("Xamarin_DeleteUser")]
        [System.Web.Http.Route("api/Login")]
        public HttpResponseMessage Xamarin_DeleteUser()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                _db.DeleteUser(userId);
                return Request.CreateResponse(HttpStatusCode.Accepted, "User deleted");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XamarinLogin.Models;
using Shared_Model_nearySport;
using Shared_Model_nearySport.Model;

namespace XamarinLogin.Controllers
{
    [NotificationFilter]
    /// <summary>
    /// NotificationController
    /// </summary>
    public class NotificationController : ApiController
    {
        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();

        /// <summary>
        ///     Get User's Notifications
        /// </summary>
        [HttpGet]
        [ActionName("XAMARIN_SelectNotifications")]
        [Route("api/Notification")]
        public HttpResponseMessage Xamarin_SelectNotification()
        {
            try
            {
                var listNotifications = new List<Tuple<string, int>>();
                var token = Request.Headers.Authorization.Parameter;
                if (token == null) return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Token");
                var tokVal = TokenNotificationManager.ValidateToken(token);
                if (tokVal == null) return Request.CreateResponse(HttpStatusCode.NotFound, "Session expired");
                var userId = int.Parse(tokVal);
                var myEvents = _db.getMyEventsNotifications(userId);
                var myEventsToRate = _db.getMyEventsNotificationsAdmin(userId);
                foreach (var ev in myEvents)
                    listNotifications.Add(new Tuple<string, int>(
                        "You have an Event coming soon : " + ev.Name + ", at : " + ev.DateEvent.Hour + ":" +
                        ev.DateEvent.Minute + ", in " + ev.Place +
                        " More informations in NearySport > MyEvents ", 0));
                foreach (var evt in myEventsToRate)
                    listNotifications.Add(new Tuple<string, int>(
                        "Your Event : " + evt.Name +
                        " is finished, please don't forget to rate the participants, Go to NearySport > MyEvents > Manage > See Participants",
                        1));
                if (listNotifications.Count == 0)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No Notifications available");
                return Request.CreateResponse(HttpStatusCode.OK, listNotifications);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
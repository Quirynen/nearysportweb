﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XamarinLogin.Models;
using XamarinLogin.Utils;
using Shared_Model_nearySport;
using Shared_Model_nearySport.Model;

namespace XamarinLogin.Controllers
{
    /// <summary>
    ///     Controller for the user's profil image
    /// </summary>
    [JWTAuthenticationFilter]
    public class ImageController : ApiController
    {
        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();

        /// <summary>
        ///     Get Profil Image for the user making the call
        /// </summary>
        [HttpGet]
        [Route("api/Image")]
        [ActionName("Xamarin_GetImage")]
        public HttpResponseMessage Xamarin_getImage()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var img = _db.getImageProfil(userId).FirstOrDefault();
                if (img == null) return Request.CreateResponse(HttpStatusCode.NotFound, "No image for this user");

                var img64 = Convert.ToBase64String(img);
                return Request.CreateResponse(HttpStatusCode.OK, img64);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Get Profil Image for a givenUserId
        /// </summary>
        [HttpGet]
        [Route("api/Image/{id}")]
        [ActionName("Xamarin_GetImage")]
        public HttpResponseMessage Xamarin_getImage([FromUri] int id)
        {
            try
            {
                var img = _db.getImageProfil(id).First();
                if (img == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No image for this user");
                return Request.CreateResponse(HttpStatusCode.OK, img);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Update profil Image, if no image is found, add a new one
        /// </summary>
        [HttpPut]
        [ActionName("Xamarin_UpdateImage")]
        [Route("api/Image")]
        public HttpResponseMessage Xamarin_UpdateImage([FromBody] string img)
        {
            try
            {
                if (img != null)
                {
                    var image = Convert.FromBase64String(img);
                    var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                    if (_db.getImageProfil(userId).Any())
                    {
                        _db.UpdateImageProfil(userId, image);
                        _db.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.Created, "Image updated");
                    }

                    var imgProfil = new ImageProfil
                    {
                        userId = userId,
                        profilImage = image
                    };
                    _db.ImageProfil.Add(imgProfil);
                    _db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Created, "Image updated");
                }

                return Request.CreateResponse(HttpStatusCode.NotFound, "Image not found");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        /// <summary>
        ///     Delete Profil Image
        /// </summary>
        [HttpDelete]
        [Route("api/Image")]
        [ActionName("Xamarin_DeleteImage")]
        public HttpResponseMessage Xamarin_DeleteImage()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                if (_db.getImageProfil(userId).FirstOrDefault() != null)
                {
                    _db.DeleteImageProfil(userId);
                    _db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Image deleted");
                }

                return Request.CreateResponse(HttpStatusCode.NotFound, "No Image to delete");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using XamarinLogin.Models;
using XamarinLogin.Utils;
using Shared_Model_nearySport;
using Shared_Model_nearySport.Model;

namespace XamarinLogin.Controllers
{
    [JWTAuthenticationFilter]
    /// <summary>
    /// MessageController
    /// </summary>
    public class MessagesController : ApiController
    {
        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();

        /// <summary>
        ///     Create new Message
        /// </summary>
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/Messages")]
        [System.Web.Http.ActionName("XAMARIN_Messages")]
        public HttpResponseMessage Xamarin_MessageInsert(MessageGrp msg)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
            }
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                if (msg != null)
                {
                    var message = new Messages
                    {
                        MessageContent = msg.MessageContent,
                        MessageFrom = userId,
                        time_Send = DateTime.Now.ToUniversalTime()
                    };
                    if (msg.MessageContent.Length < 0)
                        return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Message can not be empty");

                    if (ModelState.IsValid)
                    {
                        try
                        {
                            if (_db.getParticipationById(userId, msg.idEvent).Any())
                            {
                                _db.Messages.Add(message);
                                _db.SaveChanges();
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                                    "You have to be a participant to send messages");
                            }
                        }
                        catch (DbEntityValidationException ex)
                        {
                            return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Message not added");
                        }

                        var controller = DependencyResolver.Current.GetService<MessageGroupEventController>();
                        controller.ControllerContext = ControllerContext;
                        HttpResponseMessage response;
                        var gpmsg = new GroupEventMessage
                        {
                            idEvent = msg.idEvent,
                            idMessage = message.id
                        };
                        response = controller.XAMARIN_GrpMessages(gpmsg);
                        if (response.StatusCode == HttpStatusCode.Created)
                        {
                            var messa = _db.getMessagesById(message.id).FirstOrDefault()
                                ;
                            messa.MessageFrom = 1;
                            return Request.CreateResponse(HttpStatusCode.Created, messa);
                        }

                        _db.DeleteMessage(msg.id);
                        return Request.CreateResponse(HttpStatusCode.Conflict,
                            response.Content.ReadAsStringAsync().Result);
                    }

                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, "You have to enter a valid message");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
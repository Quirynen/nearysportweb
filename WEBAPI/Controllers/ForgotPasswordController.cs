﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using XamarinLogin.Models;
using XamarinLogin.Utils;
using Shared_Model_nearySport;
using Shared_Model_nearySport.Model;

namespace XamarinLogin.Controllers
{
    /// <summary>
    ///     ForgotPasswordController
    /// </summary>
    public class ForgotPasswordController : ApiController
    {
        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();

        /// <summary>
        ///     Add new Guid in forgotPasswordTable
        /// </summary>
        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("XAMARIN_ForgotPasswordInsert")]
        [System.Web.Http.Route("api/ForgotPassword/{Email}")]
        public HttpResponseMessage Xamarin_InsertInForgotPassword(string email)
        {
            try
            {
                if (_db.CheckMailExist(email).Any())
                {
                    var userId = int.Parse(_db.getUserIdFromEmail(email).FirstOrDefault().ToString());
                    var g = Guid.NewGuid();

                    var fp = new forgot_password
                    {
                        id = g.ToString(),
                        userId = userId,
                        timeForgot = DateTime.Now
                    };
                    _db.forgot_password.Add(fp);
                    _db.SaveChanges();
                    Util.sendMail(email, g.ToString());
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Email sent");
                }

                return Request.CreateResponse(HttpStatusCode.Accepted, "Email sent");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Get guid by id in forgotPasswordTable
        /// </summary>
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("XAMARIN_ForgotPasswordGet")]
        [System.Web.Http.Route("api/ForgotPassword/{guid}")]
        public HttpResponseMessage Xamarin_GetForgotPassword([FromRoute] string guid)
        {
            try
            {
                if (_db.getIdForgotPassword(guid).Count()>0)
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Choose a new password");
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Wrong code, please try again");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
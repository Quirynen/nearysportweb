﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using Geocoding;
using Geocoding.Google;
using Shared_Model_nearySport;
using Shared_Model_nearySport.Model;
using XamarinLogin.Models;
using XamarinLogin.Utils;

namespace XamarinLogin.Controllers
{
    /// <summary>
    ///     EventController
    /// </summary>
    [JWTAuthenticationFilter]
    public class EventController : ApiController
    {

        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();

        /// <summary>
        ///     Add New Event
        /// </summary>
        [HttpPost]
        [ActionName("XAMARIN_Event")]
        [Route("api/Events")]
        public HttpResponseMessage Xamarin_InsertEvent(Events Event)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
                }
                if (Event != null)
                {
                    try
                    {
                        var dateEvent = Event.DateEvent.ToUniversalTime();
                        var now = DateTime.Now.ToUniversalTime();
                        if ( DateTime.Compare( dateEvent.AddMinutes(-Event.CanLeaveUntil), now)<0)
                            return Request.CreateResponse(HttpStatusCode.BadRequest,
                                "You cannot not have that limit, choose a smaller's one");
                        // On reçoit l'adresse, on la convertit en lat/long et ensuite on fait l'update du dbGeography dans la database via sp
                        IGeocoder geocoder = new GoogleGeocoder
                        { ApiKey = WebConfigurationManager.AppSettings["gogoleapi"] };
                        var addresses = geocoder.Geocode(Event.Place);
                        var lon = addresses.First().Coordinates.Latitude.ToString().Replace(",", ".");
                        var lat = addresses.First().Coordinates.Longitude.ToString().Replace(",", ".");
                        double latit;
                        double longitu;
                        double.TryParse(lat, NumberStyles.Any, CultureInfo.InvariantCulture, out latit);
                        double.TryParse(lon, NumberStyles.Any, CultureInfo.InvariantCulture, out longitu);
                        if (latit <= (-84) || latit >= 84 || longitu >180 || longitu < -180)
                            return Request.CreateResponse(HttpStatusCode.NotFound,
                                "Check the address, it seems like it does not exist");
                        var positionUser = DbGeography.PointFromText(string.Format("POINT({0} {1})", lon, lat),
                            DbGeography.DefaultCoordinateSystemId);
                        Event.geolocation = positionUser;
                        Event.DateEvent = dateEvent;
                        Event.idAdmin = userId;
                        _db.Events.Add(Event);
                        _db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotAcceptable,
                            "An error occured, Check if the hour of begin and end are not the same, and the Date ");
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, "Event created");
                }

                return Request.CreateResponse(HttpStatusCode.NotFound, "Event cannot be null");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        ///     Update an existing event
        /// </summary>
        [HttpPut]
        [ActionName("Xamarin_UpdateEvents")]
        [Route("api/Events")]
        public HttpResponseMessage Xamarin_UpdateEvent(Events evenement)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
                }
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                    var ev = _db.getEventsByid(evenement.EventId).First();
                if (ev.Rated == true)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                                                   "You cannot update an event already rated");
                }
                    int idAdmin = ev.idAdmin;
                    //On checke si la nouvelle date est acceptable avec la limite de résignation
                    var dateEvent = evenement.DateEvent.ToUniversalTime();
                    var now = DateTime.Now.ToUniversalTime();
                    if (DateTime.Compare(dateEvent.AddMinutes(-ev.CanLeaveUntil), now) < 0)
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                            "You cannot not have that limit, choose a smaller's one");
                    //Pour la comparaison des dates, on le fait au niveau universalTime
                    if (evenement != null && DateTime.Compare( ev.DateEvent, DateTime.Now.ToUniversalTime()) > 0)
                    {
                        if (idAdmin != userId)
                            return Request.CreateResponse(HttpStatusCode.Unauthorized,
                                "You cannot update an event that's not yours");

                        IGeocoder geocoder = new GoogleGeocoder { ApiKey = WebConfigurationManager.AppSettings["gogoleapi"] };
                        var addresses = geocoder.Geocode(evenement.Place);
                        var lon = addresses.First().Coordinates.Latitude.ToString().Replace(",", ".");
                        var lat = addresses.First().Coordinates.Longitude.ToString().Replace(",", ".");
                        double latit;
                        double longitu;
                        double.TryParse(lat, NumberStyles.Any, CultureInfo.InvariantCulture, out latit);
                        double.TryParse(lon, NumberStyles.Any, CultureInfo.InvariantCulture, out longitu);
                        if (latit <= (-84) || latit >= 84 || longitu > 180 || longitu < -180)
                            return Request.CreateResponse(HttpStatusCode.NotFound,
                                "Check the address, it seems like it does not exist");
                        var positionEvent = DbGeography.PointFromText(string.Format("POINT({0} {1})", lon, lat),
                            DbGeography.DefaultCoordinateSystemId);
                        _db.UpdateEvent(evenement.EventId, evenement.Name, evenement.NumberParticipant, evenement.Sport,
                            evenement.Place, evenement.DateEvent, evenement.Description, evenement
                                .Cost, evenement.Duration, evenement.Required_Equipment, evenement.Rated, positionEvent, userId);
                        _db.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.Accepted, "Event updated");
                    }

                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Event not updated");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Return each events present in the table event, group by sport
        /// </summary>
        [HttpGet]
        [ActionName("XAMARIN_SelectEvent")]
        [Route("api/Events")]
        public HttpResponseMessage Xamarin_SelectAllEventBySport()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var eventsList = _db.getAllEvents();
                if (eventsList == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No Events available");
                return Request.CreateResponse(HttpStatusCode.Accepted, eventsList);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        ///     Return each events that the user administrates
        /// </summary>
        [HttpGet]
        [ActionName("XAMARIN_SelectEvent")]
        [Route("api/Events/Admin/{idUser}")]
        public HttpResponseMessage Xamarin_SelectAdminEvents([FromUri] int idUser)
        {
            try
            {
                var idAdmin = Util.getUserId(Request.Headers.Authorization.Parameter);
                var eventsList = _db.getMyAdminEvents(idAdmin, idUser);
                if (eventsList == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No Events available");
                return Request.CreateResponse(HttpStatusCode.Accepted, eventsList);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

      

        /// <summary>
        ///     Return events around the user, with the number of participants, if the user already participates or if the users is the admin
        /// </summary>
        [HttpGet]
        [ActionName("XAMARIN_SelectEvent")]
        [Route("api/Events/AroundUser")]
        public HttpResponseMessage Xamarin_SelectAllEventAroundWithNbParticipants()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                //Récupérer infos dans custom headers
                var latitude = Request.Headers.GetValues("latitude").FirstOrDefault();
                var longitude = Request.Headers.GetValues("longitude").FirstOrDefault();
                var sports = Request.Headers.GetValues("sports").FirstOrDefault();
                var nbKm = int.Parse(Request.Headers.GetValues("km").FirstOrDefault());
                if (sports == null || sports == "")
                    return Request.CreateResponse(HttpStatusCode.NotFound, "You have to check at least one sport");
                if (nbKm == 0) nbKm = nbKm + 1;
                if (nbKm < 0) return Request.CreateResponse(HttpStatusCode.NotFound, "Wrong kilometers");
                double latit;
                double longitu;
                double.TryParse(latitude, NumberStyles.Any, CultureInfo.CreateSpecificCulture("fr-FR"), out latit);
                double.TryParse(longitude, NumberStyles.Any, CultureInfo.CreateSpecificCulture("fr-FR"), out longitu);
                var eventsList = _db.getAllEventsNbParticipantsParticipation(latit, longitu, nbKm * 1000, sports,userId);
                if (eventsList == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "No Events available around, try to increase the kilometers");
                return Request.CreateResponse(HttpStatusCode.Accepted, eventsList);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }     
        /// <summary>
        /// Get every participations in the Event
        /// </summary>
        [HttpGet]
        [ActionName("XAMARIN_SelectEvent")]
        [Route("api/Events/{idEvent}/Participation")]
        public HttpResponseMessage Xamarin_SelectParticipationsByEvent(int idEvent)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var participations = _db.getPartipationsEvent(idEvent).ToList();
                var participationList = new List<getPartipationsEvent_Result>();
                if (participations == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "No Participations available for this event");

                foreach (var participation in participations)
                {
                    if (participation.idAdmin == participation.UserId)
                    {
                        participation.idAdmin = 1;
                    }
                    else
                    {
                        participation.idAdmin = 0
                        ;
                    }
                    participationList.Add(participation);
                }
                return Request.CreateResponse(HttpStatusCode.Accepted, participationList);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Load 10 participations from the events, with the offset to tell from which row the data should be retrieved
        /// </summary>
        [HttpGet]
        [ActionName("XAMARIN_SelectPartEvent")]
        [Route("api/Events/{idEvent}/Participation/{Offset}")]
        public HttpResponseMessage Xamarin_SelectParticipationsByEventOffset(int idEvent, int offset)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var participations = _db.getPartipationsEventAsync(offset, idEvent).ToList();
                var participationList = new List<getPartipationsEventAsync_Result>();
                if (participations == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "No  more Participations available for this event");

                foreach (var participation in participations)
                {
                    if (participation.idAdmin == participation.UserId)
                    {
                        participation.idAdmin = 1;
                    }
                    else
                    {
                        participation.idAdmin = 0
                        ;
                    }
                    participationList.Add(participation);
                }

                return Request.CreateResponse(HttpStatusCode.Accepted, participationList);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///  Get Event by id
        /// </summary>
        [HttpGet]
        [ActionName("XAMARIN_SelectEventById")]
        [Route("api/Events/{id}")]
        public HttpResponseMessage Xamarin_SelectEventByid(int id)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                if (id.ToString() != null)
                {
                    var evt = _db.getEventsByid(id).First();
                    if (evt != null)
                        return Request.CreateResponse(HttpStatusCode.Accepted, evt);
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No event found with this id");
                }

                return Request.CreateResponse(HttpStatusCode.NotFound, "No Events Found");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

         /// <summary>
        ///     Delete an Event by id
        /// </summary>
        [HttpDelete]
        [ActionName("Xamarin_DeleteEvents")]
        [Route("api/Events/{id}")]
        public HttpResponseMessage Xamarin_DeleteEvent(int id)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                if (id != 0)
                {
                    var _event = _db.getEventsByid(id).First();
                    if (_event == null)
                        return Request.CreateResponse(HttpStatusCode.NotFound,
                            "Wrong Event");
                    if (_event.idAdmin != userId)
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            "You cannot delete an event that's not yours");
                    }

                    _db.DeleteEvent(id, userId);
                    _db.DeleteAllParticipations(id);
                    _db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Event Deleted");
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, "Event not Deleted");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
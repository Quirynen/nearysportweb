﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using XamarinLogin.Models;
using XamarinLogin.Utils;
using Shared_Model_nearySport;
using Shared_Model_nearySport.Model;

namespace XamarinLogin.Controllers
{
    /// <summary>
    ///     ParticipationsController
    /// </summary>
    [JWTAuthenticationFilter]
    public class ParticipationsController : ApiController
    {
        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();

        /// <summary>
        ///     Add a new Participation to an event
        /// </summary>
        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("XAMARIN_Participation")]
        [System.Web.Http.Route("api/MyParticipations")]
        public HttpResponseMessage Xamarin_InsertParticipation(Participations participation)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
            }
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var ev = _db.getEventsByid(participation.IdEvent).First();
                if (DateTime.Compare(ev.DateEvent, DateTime.Now.ToUniversalTime()) < 0) return Request.CreateResponse(HttpStatusCode.BadRequest,
                                 "You cannot add a participation in this event anymore");
                if (userId == ev.idAdmin)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                                  "You cannot add a participation in your own event");
                }
               
                if (participation != null)
                {
                    var nbParticpations = _db.getPartipationsEvent(participation.IdEvent).Count()+1;

                    if (nbParticpations >= _db.getEventsByid(participation.IdEvent).First().NumberParticipant)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                "The Event is full, you cannot participate right now");
                    }
                    var result = _db.getParticipationById(userId, participation.IdEvent).ToList();
                    var invit = _db.SelectInvitations(ev.EventId, userId).FirstOrDefault();
                    if (result.Count == 0)
                    {
                        if (!ev.isPublic)
                        {
                            if (invit == null )
                            {
                                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                                    "You cannot participate in this event, a problem occured");
                            }
                        }
                        participation.UserId = userId;
                        _db.Participations.Add(participation);
                        _db.SaveChanges();
                        if (nbParticpations >= _db.getEventsByid(participation.IdEvent).First().NumberParticipant)
                        {
                            _db.DeleteParticipations(participation.IdEvent, userId);
                            _db.SaveChanges();
                            return Request.CreateResponse(HttpStatusCode.BadRequest,
                                  "The Event is full, you cannot participate right now");
                        }
                        if (!ev.isPublic || invit != null)
                        {
                            _db.UpdateInvitation(ev.EventId, userId, 1);
                            _db.SaveChanges();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, "Participation accepted");

                    }

                    return Request.CreateResponse(HttpStatusCode.BadRequest, "You already joined this event");
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest,
                    "Participation not creates, a problem occured");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Get User's Events
        /// </summary>
        [System.Web.Http.Route("api/MyParticipations")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("XAMARIN_SelectMyEvent")]
        public HttpResponseMessage Xamarin_SelectMyEvents()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var myEvents = _db.getMyEvents(userId).ToList();

                foreach (var ev in myEvents)
                {
                    if (ev.idAdmin == userId)
                    {
                        ev.idAdmin = 1;
                    }
                    else
                    {
                        ev.idAdmin = 0;
                    }
                }
                if (myEvents == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No Events available");
                return Request.CreateResponse(HttpStatusCode.Accepted, myEvents);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        ///     Get User's Events with more infos
        /// </summary>
        [System.Web.Http.Route("api/MyParticipations/Details")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("XAMARIN_SelectMyEventInfos")]
        public HttpResponseMessage Xamarin_SelectMyEventsInfos()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var myEvents = _db.getMyEventsInfos(userId).ToList();
                foreach (var ev in myEvents)
                {
                    if (ev.idAdmin == userId)
                    {
                        ev.idAdmin = 1;
                    }
                    else
                    {
                        ev.idAdmin = 0;
                    }
                }
                if (myEvents == null || myEvents.Count==0)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No Events available");
                return Request.CreateResponse(HttpStatusCode.Accepted, myEvents);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        ///     Get User's Role in an Event, the event is retrieved by id
        /// </summary>
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("XAMARIN_SelectRole")]
        [System.Web.Http.Route("api/MyParticipations/{idEvent}")]
        public HttpResponseMessage Xamarin_SelectRole(int idEvent)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var _event = _db.getEventsByid(idEvent).FirstOrDefault();
                if (_event == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No Event Found");
                if (userId == _event.idAdmin) return Request.CreateResponse(HttpStatusCode.Accepted, 1);
                return Request.CreateResponse(HttpStatusCode.Accepted, 0);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }


        /// <summary>
        ///     Update the statut of a participation, if the user is the admin
        /// </summary>
        [System.Web.Http.HttpPut]
        [System.Web.Http.ActionName("XAMARIN_UpdateStatut")]
        [System.Web.Http.Route("api/MyParticipations/{idEvent}")]
        public HttpResponseMessage Xamarin_UpdateStatut([FromRoute] int idEvent, [System.Web.Http.FromBody] List<Tuple<int, bool>> listUsersStatut)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var _event = _db.getEventsByid(idEvent).FirstOrDefault();
                var dateNow = DateTime.Now.ToUniversalTime();
                if (DateTime.Compare(dateNow, _event.DateEvent) < 0)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                                       "You cannnot update this Event now");
                }
                if (_event == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                  "Event not found");
                if (_event.idAdmin == userId)
                {
                    foreach (var userStat in listUsersStatut)
                    {
                        if (userStat.Item1 != _event.idAdmin)
                        {
                            _db.UpdateStatutParticipation(userStat.Item1, idEvent, userStat.Item2);
                            _db.SaveChanges();
                        }
                        //C'est l'administrateur, donc pas de participations dans la table participations, donc pas de trigger enclenché
                        else
                        {
                            var profilAdmin = _db.getProfil(userStat.Item1).FirstOrDefault();                           
                            if (userStat.Item2) //L'admin a bien participé, peu de chances du contraire mais sait-on jamais
                            {
                                double fiability = (Double.Parse((profilAdmin.NbEventParticipated + 1).ToString()) / (Double.Parse((profilAdmin.NbEventRegistered + 1).ToString())))*10;
                                _db.UpdateFiability(fiability, profilAdmin.NbEventParticipated + 1, profilAdmin.NbEventRegistered + 1, userStat.Item1);
                                _db.SaveChanges();
                            }
                            else //Pas venu
                            {
                                double fiability = (Double.Parse((profilAdmin.NbEventParticipated ).ToString()) / (Double.Parse((profilAdmin.NbEventRegistered + 1).ToString()))) * 10;
                                _db.UpdateFiability(fiability, profilAdmin.NbEventParticipated , profilAdmin.NbEventRegistered + 1, userStat.Item1);
                                _db.SaveChanges();
                            }
                        }                      
                    }
                    _db.UpdateRateEvent(idEvent, userId);
                    _db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Status Updated");
                }
                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    "You cannnot update an Event that's not yours");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Delete an existing participation in an event
        /// </summary>
        [System.Web.Http.HttpDelete]
        [System.Web.Http.ActionName("Xamarin_DeleteParticipations")]
        [System.Web.Http.Route("api/MyParticipations/{id}")]
        public HttpResponseMessage Xamarin_DeleteParticipation(int id)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var ev = _db.getEventsByid(id).First();
                var dateEvent = ev.DateEvent.ToUniversalTime();
                var now = DateTime.Now.ToUniversalTime();
                if (DateTime.Compare(ev.DateEvent, DateTime.Now.ToUniversalTime()) < 0) return Request.CreateResponse(HttpStatusCode.BadRequest, "You may no longer unregister from this event");
                if (ev.idAdmin == userId)
                {
                    _db.DeleteEvent(id, userId);
                    _db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Event Deleted");
                }
                var result = _db.getParticipationById(userId, id).ToList();
                var nbParticipation = _db.getPartipationsEvent(id).Count();
                if (result.Count == 0) return Request.CreateResponse(HttpStatusCode.NotFound, "Event does not exist");
                _db.DeleteParticipations(id, userId);
                _db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.Accepted, "Participation Deleted");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
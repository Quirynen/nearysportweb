﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XamarinLogin.Models;
using XamarinLogin.Utils;
using Shared_Model_nearySport;
using Shared_Model_nearySport.Model;

namespace XamarinLogin.Controllers
{
    [JWTAuthenticationFilter]
    /// <summary>
    /// ProfilController
    /// </summary>
    public class ProfilController : ApiController
    {
        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();

        // POST: api/Login  
        /// <summary>
        ///     Add a new Profil
        /// </summary>
        [HttpPost]
        [Route("api/Profil")]
        [ActionName("XAMARIN_PROFIL")]
        [AllowAnonymous]
        public HttpResponseMessage Xamarin_regProfil(Profil profil)
        {
            try
            {
                //Check si mail présent dans Database
                if (profil != null)
                {
                    var result = _db.CheckMailExist(profil.Mail);
                    if (result.Any())
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                            "Mail already took by another account, try another one");

                    if (!ModelState.IsValid)
                        return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                    try
                    {
                        _db.Profil.Add(profil);
                        _db.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.Created, "Profil created");
                    }
                    catch (DbEntityValidationException ex)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Profil not added");
                    }
                }

                return Request.CreateResponse(HttpStatusCode.NotFound, "Profil cannot be null");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Profil  
        /// <summary>
        ///     Update an existing profil
        /// </summary>
        [HttpPut]
        [Route("api/Profil")]
        [ActionName("XAMARIN_UpdateProfil")]
        public HttpResponseMessage Xamarin_UpdateProfil(Profil profil)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
            }
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                if (profil != null)
                {
                    var email = _db.getProfil(userId).First().Mail;
                    var result = _db.CheckMailExist(profil.Mail);
                    if (result.Any() && !profil.Mail.Equals(email))
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                            "Mail already took by another account, try another one");
                    _db.UpdateProfil(profil.Town, profil.Mail, userId);
                    _db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Profil updated");
                }

                return Request.CreateResponse(HttpStatusCode.NotFound, "Please enter a valid Profil");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Get User's Profil by JWT
        /// </summary>
        [HttpGet]
        [Route("api/Profil/")]
        [ActionName("XAMARIN_SelectProfil")]
        public HttpResponseMessage Xamarin_SelectProfil()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var profilResult = _db.getProfil(userId).SingleOrDefault();
                var profilret = new ProfilUser
                {
                    UserName = profilResult.UserName,
                    Password = profilResult.Password,
                    Email = profilResult.Mail,
                    Town = profilResult.Town,
                    Fiability = profilResult.Fiability,
                    NbEventRegistered = profilResult.NbEventRegistered,
                    NbEventsParticipated = profilResult.NbEventParticipated
                };
                if (profilret == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No User Profil Found");
                return Request.CreateResponse(HttpStatusCode.Accepted, profilret);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Get user's profil with a matching username
        /// </summary>
        [HttpGet]
        [Route("api/Profil/{username}")]
        [ActionName("XAMARIN_SelectUsers")]
        public HttpResponseMessage Xamarin_SelectUsers([FromUri]string username)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var listUsers = _db.SelectUsers(username).ToList();
                if (!listUsers.Any())
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No User Profil Found");
                return Request.CreateResponse(HttpStatusCode.Accepted, listUsers);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        /// Get first user's profil with a matching username
        /// </summary>
        [HttpGet]
        [Route("api/Profil/5/{username}")]
        [ActionName("XAMARIN_SelectFirstUsers")]
        public HttpResponseMessage Xamarin_SelectFirstUsers([FromUri]string username)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                var listUsers = _db.SelectFirstUsers(username).ToList();
                if (!listUsers.Any())
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No User Profil Found");
                return Request.CreateResponse(HttpStatusCode.Accepted, listUsers);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Login  
        /// <summary>
        ///     Delete an existing profil
        /// </summary>
        [HttpDelete]
        [ActionName("Xamarin_DeleteProfil")]
        [Route("api/Profil")]
        public HttpResponseMessage Xamarin_DeleteProfil()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                _db.DeleteProfil(userId);
                return Request.CreateResponse(HttpStatusCode.Accepted, "Profil deleted");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
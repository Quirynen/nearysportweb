﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using XamarinLogin.Models;
using XamarinLogin.Utils;
using Shared_Model_nearySport;
using Shared_Model_nearySport.Model;

namespace XamarinLogin.Controllers
{
    [JWTAuthenticationFilter]
    /// <summary>
    /// MessageGroupEventController
    /// </summary>
    public class MessageGroupEventController : ApiController
    {
        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();

        /// <summary>
        ///     Add new GroupMessage
        /// </summary>
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/MessagesGroup")]
        [System.Web.Http.ActionName("XAMARIN_GrpMessages")]
        public HttpResponseMessage XAMARIN_GrpMessages(GroupEventMessage msg)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
            }
            try
            {
                if (msg != null)
                {
                    if (!ModelState.IsValid)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
                    }
                    try
                    {
                        msg.nbMessage = msg.nbMessage + 1;
                        _db.GroupEventMessage.Add(msg);
                        _db.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.Created, "Message added");
                    }
                    catch (DbEntityValidationException ex)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Message not added");
                    }
                }

                return Request.CreateResponse(HttpStatusCode.Unauthorized, "Message cannot be null");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Return each message for an Event
        /// </summary>
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("XAMARIN_SelectMessagesEvent")]
        [System.Web.Http.Route("api/MessagesGroup/{idEvent}")]
        public HttpResponseMessage Xamarin_SelectAllMessagesEvent([FromRoute] int idEvent)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                if (_db.getParticipationById(userId, idEvent).Any())
                {
                    var messages = _db.getMessagesEvent(idEvent);
                    var messagesToReturn = new List<getMessagesEvent_Result>();
                    //MessageFrom joue le rôle d'un booléen, il détermine si le message appartient à l'utilisateur qui consulte
                    foreach (var m in messages)
                    {
                        if (m.MessageFrom == userId)
                            m.MessageFrom = 1;
                        else
                            m.MessageFrom = 0;
                        messagesToReturn.Add(m);
                    }

                    return Request.CreateResponse(HttpStatusCode.Accepted, messagesToReturn);
                }

                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    "You are not authorized to see the messages in this Event");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///     Return each message for an Event, Loaded step by step, not all at once
        /// </summary>
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("XAMARIN_SelectMessagesEvent")]
        [System.Web.Http.Route("api/MessagesGroup/Async/{idEvent}/{idMessage}")]
        public HttpResponseMessage Xamarin_SelectAsyncMessages([FromRoute] int idEvent, [FromRoute] int idMessage)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                if (_db.getParticipationById(userId, idEvent).Any())
                {
                    var messages = _db.loadMessagesAsync(idEvent, idMessage);
                    var messagesToReturn = new List<loadMessagesAsync_Result>();
                    //MessageFrom joue le rôle d'un booléen, il détermine si le message appartient à l'utilisateur qui consulte
                    foreach (var m in messages)
                    {
                        if (m.MessageFrom == userId)
                            m.MessageFrom = 1;
                        else
                            m.MessageFrom = 0;
                        messagesToReturn.Add(m);
                    }

                    return Request.CreateResponse(HttpStatusCode.Accepted, messagesToReturn);
                }

                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    "You are not authorized to see the messages in this Event");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        ///   Get call for polling, to check new messages
        /// </summary>
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("XAMARIN_checkMessagesEvent")]
        [System.Web.Http.Route("api/MessagesGroup/polling/{idEvent}/{idMessage}")]
        public HttpResponseMessage Xamarin_CheckMessagesEvent([FromRoute] int idEvent, [FromRoute] int idMessage)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                if (_db.getParticipationById(userId, idEvent).Any())
                {
                    var messages = _db.checkMessages(idEvent, idMessage);
                    var messagesToReturn = new List<checkMessages_Result>();
                    //MessageFrom joue le rôle d'un booléen, il détermine si le message appartient à l'utilisateur qui consulte
                    foreach (var m in messages)
                    {
                        if (m.MessageFrom == userId)
                            m.MessageFrom = 1;
                        else
                            m.MessageFrom = 0;
                        messagesToReturn.Add(m);
                    }

                    if (messages == null)
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "No more Messages available");
                    return Request.CreateResponse(HttpStatusCode.Accepted, messagesToReturn);
                }

                return Request.CreateResponse(HttpStatusCode.Unauthorized,
                    "You are not authorized to see the messages in this Event");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
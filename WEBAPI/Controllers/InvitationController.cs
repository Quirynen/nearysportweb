﻿using Shared_Model_nearySport.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XamarinLogin.Models;
using XamarinLogin.Utils;

namespace XamarinLogin.Controllers
{
    public class InvitationController : ApiController
    {
        private readonly NearySport_DBEntities _db = new NearySport_DBEntities();
        /// <summary>
        ///     Get Invitation for an Event
        /// </summary>
        [System.Web.Http.Route("api/Invitations/Admin")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("XAMARIN_SelectInvitationsForEvent")]
        public HttpResponseMessage Xamarin_SelectInvitationsForEvent()
        {
            try
            {
                var idAdmin = Util.getUserId(Request.Headers.Authorization.Parameter);
                var invitations = _db.SelectInvitationsEventsAdmin(idAdmin);
                return Request.CreateResponse(HttpStatusCode.Accepted, invitations);

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        ///     Get Invitation for the user that make the API call
        /// </summary>
        [System.Web.Http.Route("api/Invitations")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("XAMARIN_SelectMyInvitations")]
        public HttpResponseMessage Xamarin_SelectMyInvitations()
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                //SP getInvitationsForEvent
                var invitations = _db.SelectMyInvitations(userId);
                return Request.CreateResponse(HttpStatusCode.Accepted, invitations);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        ///     Add a new Invitation for a given Event, userId
        /// </summary>
        [System.Web.Http.Route("api/Invitations")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("XAMARIN_AddInvitations")]
        public HttpResponseMessage Xamarin_AddInvitations(List<Invitation> listInvitation)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
            }
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                foreach (var invit in listInvitation)
                {
                    int idAdmin = _db.getEventsByid(invit.idEvent).First().idAdmin;
                    if (userId != idAdmin)
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, "You are not authorized to send invitations for this Event");
                    }
                    else
                    {
                        var hisEvents = _db.getMyEvents(invit.idUser).ToList();
                        foreach(var ev in hisEvents)
                        {
                            if (ev.EventId == invit.idEvent)
                            {
                                return Request.CreateResponse(HttpStatusCode.Unauthorized, "Invitation not available for this user");
                            }
                        }
                        var invitAlreadyThere = _db.CheckInvitationAlreadyThere(invit.idEvent, invit.idUser).FirstOrDefault();
                        if (invitAlreadyThere != null)
                        {
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, "Invitation already sended for this user");
                        }
                        invit.statutInvitation = 2; //2=pending, en attente de confirmation
                        _db.Invitation.Add(invit);
                    }
                }
                _db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, "The invitations have been sent");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        ///     Delete an Invitation for a given Event, userId
        /// </summary>
        [System.Web.Http.Route("api/Invitations/{idEvent}/{idUser}")]
        [System.Web.Http.HttpDelete]
        [System.Web.Http.ActionName("XAMARIN_DeleteInvitations")]
        public HttpResponseMessage Xamarin_DeleteIvitations([FromUri] int idEvent, [FromUri] int idUser)
        {
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                int idAdmin = _db.getEventsByid(idEvent).First().idAdmin;
                if (userId != idAdmin)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "You are not authorized to delete invitations for this Event");
                }
                else
                {
                    _db.DeleteInvitation(idEvent, idUser);
                    return Request.CreateResponse(HttpStatusCode.OK, "The invitation has been deleted");
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        ///     Update the statut of an Invitation for a given Event, userId
        /// </summary>
        [System.Web.Http.Route("api/Invitations")]
        [System.Web.Http.HttpPut]
        [System.Web.Http.ActionName("XAMARIN_UpdateInvitations")]
        public HttpResponseMessage Xamarin_UpdateInvitations(Invitation invitation)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.First().Errors[0].ErrorMessage);
            }
            try
            {
                var userId = Util.getUserId(Request.Headers.Authorization.Parameter);
                _db.UpdateInvitation(invitation.idEvent, userId, invitation.statutInvitation);
                return Request.CreateResponse(HttpStatusCode.OK, "The invitation has been Updated");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.InnerException.Message);
            }
        }
    }
}

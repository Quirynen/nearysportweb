using System.Web.Http;
using WebActivatorEx;
using XamarinLogin;
using Swashbuckle.Application;
using System.Linq;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace XamarinLogin
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        
                        c.SingleApiVersion("v1", "XamarinLogin");
                        c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());

                        c.IncludeXmlComments(string.Format(@"{0}\bin\XamarinLogin.xml", System.AppDomain.CurrentDomain.BaseDirectory));
                    })
                .EnableSwaggerUi(c =>
                    {
  
                    });
        }
    }
}

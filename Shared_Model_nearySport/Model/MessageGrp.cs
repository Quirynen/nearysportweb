﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_Model_nearySport.Model
{
    public class MessageGrp
    {
        public int id { get; set; }
        public int MessageFrom { get; set; }
        [Required]
        [MinLength(1, ErrorMessage = "Your message must at least contains one letter")]
        [MaxLength(256)]
        public string MessageContent { get; set; }
        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "Id can not be negative")]
        public int idEvent { get; set; }
        public DateTime time_Send { get; set; }
    }
}

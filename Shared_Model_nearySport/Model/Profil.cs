//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Shared_Model_nearySport.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Profil
    {
        public int UserId { get; set; }
        public double Fiability { get; set; }
        public string Town { get; set; }
        public string Mail { get; set; }
        public int NbEventParticipated { get; set; }
        public int NbEventRegistered { get; set; }
    
        public virtual Users Users { get; set; }
    }
}

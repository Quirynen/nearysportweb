﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_Model_nearySport.Model
{
    public class ProfilUser
    {
        [Required]
        [MinLength(3)]
        public string UserName { get; set; }
        [Required]
        [MinLength(8)]
        [MaxLength(15)]
        public string Password { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [MinLength(3)]
        [Required]
        public string Town { get; set; }
        public double Fiability { get; set; }
        public int NbEventsParticipated { get; set; }
        public int NbEventRegistered { get; set; }
    }
}

import { NgModule } from '@angular/core';
//import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './alert/alert.component';
import { AuthGuard } from './router_guards/authentification.guard';
import { JwtInterceptor } from './redirector/jwt_interceptor';
import { ErrorInterceptor } from './redirector/error_redirector';
import { AlertService } from './_services/alert.service';
import {  AuthenticationService } from './_services/nearysport_authentification.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EnvironmentUrlService } from './shared/services/environment-url.service';
import { RepositoryService } from './shared/services/repository.service';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { MatTab, MatTabLabel } from '@angular/material';
@NgModule({
  imports: [
   /* BrowserModule,*/
    ReactiveFormsModule,
    HttpClientModule,
    routing, /*BrowserAnimationsModule ,MatTab, MatTabLabel*/
  ],
  declarations: [
    AppComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent
  ],
  providers: [
    AuthGuard,
    EnvironmentUrlService,
    RepositoryService,
    AlertService,
    AuthenticationService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }

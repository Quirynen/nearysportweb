import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { RepositoryService } from '../shared/services/repository.service'
import { AlertService } from '../_services/alert.service';
import { AuthenticationService } from '../_services/nearysport_authentification.service';
import { Alert } from 'selenium-webdriver';

@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  public result: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private repo: RepositoryService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(15)]]
    });

    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;

    this.repo.getData('/api/Login/?username=' + this.f.username.value + ('&password=') + this.f.password.value)
      .subscribe(res => {
        this.result = res;
        localStorage.setItem('currentUserJWT', JSON.stringify(this.result[0]));
        alert(res);
        this.router.navigate(['/eventsfeed']);
        this.loading = false;
        //Accéder au fil d'actu
      },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}

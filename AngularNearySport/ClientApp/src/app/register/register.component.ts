import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MustMatch } from '../_services/must-match';
import { AlertService} from '../_services/alert.service';
import { UserRegister } from '../../Model/UserRegister';
import { RepositoryService } from '../shared/services/repository.service'

@Component({ templateUrl: 'register.component.html' })
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private repo: RepositoryService,
    private alertService: AlertService
) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(3)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(15)]],
      confirmpassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(15)]],
      mail: ['', Validators.required],
      town: ['', Validators.required]
    }, {
       validator : MustMatch('password', 'confirmpassword')
      });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;
    //Should I keep it like that?
    let user: UserRegister = new UserRegister(this.f.username.value,this.f.password.value,this.f.mail.value,this.f.town.value);
    let body = JSON.stringify(user)   
    localStorage.removeItem('currentUserJWT');
    this.repo.create('api/Login/CreateProfil',body)
      .subscribe(res => {
        alert(res);
        this.router.navigate(['/login']);
      },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}

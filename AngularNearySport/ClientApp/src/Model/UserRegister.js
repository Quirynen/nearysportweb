define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class UserRegister {
        constructor(userN, pass, email, location) {
            // clear alert message on route change
            this.username = userN;
            this.password = pass;
            this.Email = email;
            this.town = location;
        }
    }
    exports.UserRegister = UserRegister;
});
//# sourceMappingURL=UserRegister.js.map
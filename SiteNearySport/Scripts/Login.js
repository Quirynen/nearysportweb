﻿function Login(server) {
    $(document).ajaxStart(function () {
        $("#gifLoading").show();
    }).ajaxStop(function () {
        $("#gifLoading").hide();
    });
    var username = document.getElementById("username").value;
    $('#btnLogin').prop('disabled', true);
    var password = $('#password')[0].value;
    var entryPoint = 'api/Login?username=' + username + ('&password=') + password;
    var uri = server + entryPoint;
    $.ajax({
        type: "GET",
        url: uri,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        crossDomain: true,
        success: function (dataFromController) {
            var jwt = Cookies.get('JWT');
            if (!jwt) {
                Cookies.set('JWT', dataFromController);
            } else {
                Cookies.remove('JWT');
                Cookies.set('JWT', dataFromController);
            }
            window.location.href = '/Fil_dActu';
        },
        error: function (error) {
            alert(error.status + ": " + error.responseJSON);
        }
    });
}
